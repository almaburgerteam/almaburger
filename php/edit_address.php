<?php
    require "functions.php";
    sec_session_start();

    if(!login_check() || !isset($_POST["nome"]) || !isset($_POST["cognome"]) || !isset($_POST["indirizzo"]) || !isset($_POST["comune"]) || !isset($_POST["provincia"]) || !isset($_POST["cap"])) {
      header("location: ../index.php");
      die();
    }

    if(!empty($_POST["id"])) {
      //Aggiornamento indirizzo esistente
      $stmt = $mysqli->prepare("UPDATE indirizzi SET idUtente=?, cognome=?, nome=?, indirizzo=?, comune=?, provincia=?, cap=? WHERE id=?");
      $stmt->bind_param('dssssssd', $_SESSION['user_id'], $_POST['cognome'], $_POST['nome'], $_POST['indirizzo'], $_POST['comune'], $_POST['provincia'], $_POST['cap'], $_POST['id']);
      $stmt->execute();
      header('Location: ../profile.php?success=3');
    } else {
      //Nuovo indirizzo
      $stmt = $mysqli->prepare("INSERT INTO indirizzi(idUtente, cognome, nome, indirizzo, comune, provincia, cap) VALUES (?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('dssssss', $_SESSION['user_id'], $_POST['cognome'], $_POST['nome'], $_POST['indirizzo'], $_POST['comune'], $_POST['provincia'], $_POST['cap']);
      $stmt->execute();
      header('Location: ../profile.php?success=2');
    }
?>
