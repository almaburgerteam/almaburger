<?php
  require "functions.php";
  sec_session_start();

  if(!login_check() || !isset($_POST["idOrdine"]) || !isset($_POST["recensione"])) {
    header('Location: ../index.php');
    die();
  }

  $stmt = $mysqli->prepare("SELECT idUtente, recensione FROM ordini WHERE id = ?");
  $stmt->bind_param('d', $_POST["idOrdine"]);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($idUtente, $recensione);
  if($stmt->num_rows <= 0) {
    header('Location: ../index.php');
    die();
  }
  $stmt->fetch();
  if($idUtente != $_SESSION['user_id'] || !empty($recensione)) {
    header('Location: ../index.php');
    die();
  }

  $stmt = $mysqli->prepare("UPDATE ordini SET recensione= ? WHERE id = ?");
  $stmt->bind_param('sd', $_POST["recensione"], $_POST["idOrdine"]);
  $stmt->execute();
  header('Location: ../order.php?id='.$_POST["idOrdine"]);
?>
