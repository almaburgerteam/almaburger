<?php
  require "functions.php";
  sec_session_start();

  if(!is_admin() || !isset($_POST["idOrdine"]) || !isset($_POST["azione"])) {
    header('Location: ../index.php');
    die();
  }

  $idOrdine = $_POST['idOrdine'];
  switch ($_POST["azione"]) {
    case 'spedito':
      $titolo = "Almaburger - Ordine #$idOrdine in consegna";
      $notifica = "Il tuo è stato evaso e presto arriverà a destinazione. Buon appetito!!!";
      $stato = "IN CONSEGNA";
      break;
    case 'consegnato':
      $titolo = "Almaburger - Ordine #$idOrdine consegnato";
      $notifica = "Il tuo è stato marcato come consegnato. Vorremmo avere un feedback da parte tua, perchè non ci lasci una recensione? Puoi scriverla qui: <a href='https://almaburger.altervista.org/order.php?id=$idOrdine'>https://almaburger.altervista.org/order.php?id=$idOrdine</a>";
      $stato = "CONSEGNATO";
      break;
    default:
      return "IN ELABORAZIONE";
  }
  //Aggiorno stato ordine
  $stmt = $mysqli->prepare("UPDATE ordini SET stato= ? WHERE id = ?");
  $stmt->bind_param('sd', $stato, $_POST["idOrdine"]);
  $stmt->execute();
  //Invio notifica
  $stmt = $mysqli->prepare("SELECT idUtente, email FROM utenti INNER JOIN ordini ON ordini.idUtente = utenti.id WHERE ordini.id = ?");
  $stmt->bind_param('d', $_POST["idOrdine"]);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($idUtente, $email);
  $stmt->fetch();
  $to = $email;
  $subject = $titolo;
  $body = "<p>$notifica</p>";
  $headers = "From: noreply@almaburger.com\r\n";
  $headers .= "Reply-To: noreply@almaburger.com\r\n";
  $headers .= "Content-type: text/html; charset=UTF-8\r\n";
  $headers .= "Content-Transfer-Encoding: 8bit\r\n";
  mail($to, $subject, $body, $headers);
  $data = date('Y-m-d H:i:s');
  $stmt2 = $mysqli->prepare("INSERT INTO notifiche(idUtente, data, testo) VALUES (?, ?, ?)");
  $testo = "Ordine #$idOrdine - $notifica";
  $stmt2->bind_param('dss', $idUtente, $data, $testo);
  $stmt2->execute();

  header('Location: ../order.php?id='.$_POST["idOrdine"]);
?>
