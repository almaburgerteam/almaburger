<?php
    require 'functions.php';
    sec_session_start();

    if (login($_POST['email'], $_POST['pwd'])) {
        header('Location: ../profile.php');
    }
    else {
        header('Location: ../login.php?error=invalid');
    }
?>
