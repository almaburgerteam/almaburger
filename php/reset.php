<?php
    require 'functions.php';
    sec_session_start();

    $token = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()) , true));
    $storedToken = hash('sha512', $token);

    if(isset($_POST['email'])) {
      $email = $_POST['email'];
      $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE email = ?");
      $stmt->bind_param('s', $_POST['email']);
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows <= 0) {
        header("Location: ../profile.php?reset=1");
        die();
      }
      $stmt = $mysqli->prepare("UPDATE utenti SET token=? WHERE email=?");
      $stmt->bind_param('ss', $storedToken, $_POST['email']);
      $stmt->execute();
    } else if(login_check()) {
      $stmt = $mysqli->prepare("SELECT email FROM utenti WHERE id = ?");
      $stmt->bind_param('d', $_SESSION['user_id']);
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($email);
      $stmt->fetch();
      $stmt = $mysqli->prepare("UPDATE utenti SET token=? WHERE id=?");
      $stmt->bind_param('sd', $storedToken, $_SESSION['user_id']);
      $stmt->execute();
    } else {
      header('Location: ../index.php');
      die();
    }

    $to = $email;
    $subject = "Almaburger - Reset password";
    $body = "<p>Qualcuno ha richiesto che la password fosse resettata.</p>
    <p>Se la richiesta è stata fatta per errore allora ignora questa email.</p>
    <p>Per resettare la tua password visita il seguente indirizzo: <a href='https://almaburger.altervista.org/reset-password.php?key=$token'>http://almaburger.altervista.org/reset-password.php?key=$token</a></p>";
    $headers = "From: noreply@almaburger.com\r\n";
    $headers .= "Reply-To: noreply@almaburger.com\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "Content-Transfer-Encoding: 8bit\r\n";
    mail($to, $subject, $body, $headers);
    header("Location: ../profile.php?reset=1");
?>
