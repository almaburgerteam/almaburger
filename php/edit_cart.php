<?php
    require "functions.php";
    sec_session_start();

    if(login_check() && isset($_POST["action"]) && $_POST["action"] == "elimina") {
      $stmt = $mysqli->prepare("DELETE FROM carrelli WHERE idUtente = ? AND idProdotto = ?");
      $stmt->bind_param('dd', $_SESSION['user_id'], $_POST["idProd"]);
      $stmt->execute();
      echo calculateTotal();
    } else if(login_check() && isset($_POST["action"]) && $_POST["action"] == "modifica") {
      $stmt = $mysqli->prepare("UPDATE carrelli SET quantita=? WHERE idUtente=? AND idProdotto=?");
      $stmt->bind_param('ddd', $_POST["qta"], $_SESSION['user_id'], $_POST["idProd"]);
      $stmt->execute();
      echo calculateTotal();
    } else {
      echo "ERROR";
    }

    function calculateTotal() {
      global $mysqli;
      $totale = 0;
      $stmt = $mysqli->prepare("SELECT prezzo, quantita FROM carrelli INNER JOIN menu ON menu.id = carrelli.idProdotto WHERE idUtente = ?");
      $stmt->bind_param('d', $_SESSION['user_id']);
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows > 0) {
        $stmt->bind_result($prezzo, $quantita);
        while($stmt->fetch()) {
          $totale += $prezzo * $quantita;
        }
      }
      return $totale;
    }
?>
