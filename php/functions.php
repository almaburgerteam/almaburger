<?php
		require 'db_connect.php';

		function sec_session_start() {
		    $session_name = 'sec_session_id'; //Nome di sessione
		    $secure = true; //Usa il protocollo 'https'.
		    $httponly = true; //Impedisce a javascript di essere in grado di accedere all'id di sessione.
		    ini_set('session.use_only_cookies', 1); //Forza la sessione ad utilizzare solo i cookie.
		    $cookieParams = session_get_cookie_params(); //Legge i parametri correnti relativi ai cookie.
		    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
		    session_name($session_name); //Imposta il nome di sessione con quello prescelto all'inizio della funzione.
		    session_start(); //Avvia la sessione php.
		    session_regenerate_id(); //Rigenera la sessione e cancella quella creata in precedenza.
		}

		function login($email, $password) {
				global $mysqli;
		    if ($stmt = $mysqli->prepare("SELECT id, password, salt, email FROM utenti WHERE email = ? AND attivo = 1 LIMIT 1")) {
		        $stmt->bind_param('s', $email);
		        $stmt->execute();
		        $stmt->store_result();
		        $stmt->bind_result($user_id, $db_password, $salt, $email);
		        $stmt->fetch();
		        $password = hash('sha512', $password . $salt); //Codifica la password usando una chiave univoca.
		        if ($stmt->num_rows == 1) { //Se l'utente esiste
		            //Verifica che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
		            if (checkbrute($user_id) == true) {
		                //Account disabilitato.
		                //Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
										$to = $email;
										$subject = "Almaburger - Account disabilitato";
										$body = "<p>Se hai ricevuto questa email significa che il tuo account è stato temporaneamente disabilitato a seguito di un elevato numero di tentativi di accesso errati.</p>
										<p>Potrai ritentare l'accesso al tuo account fra 2 ore dalla presente mail.</p>
										<p>Se ritieni che il tuo account sia stato oggetto di attacchi informatici ti consigliamo inoltre di cambiare la password.</p>";
										$headers = "From: noreply@almaburger.com\r\n";
										$headers .= "Reply-To: noreply@almaburger.com\r\n";
										$headers .= "Content-type: text/html; charset=UTF-8\r\n";
										$headers .= "Content-Transfer-Encoding: 8bit\r\n";
										mail($to, $subject, $body, $headers);
		                return false;
		            }
		            else {
		                //Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
		                if ($db_password == $password) {
		                    $user_browser = $_SERVER['HTTP_USER_AGENT']; //Recupera il parametro 'user-agent' relativo all'utente corrente.
		                    $user_id = preg_replace("/[^0-9]+/", "", $user_id); //Protezione da un attacco XSS
		                    $_SESSION['user_id'] = $user_id;
		                    $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
		                    return true;
		                }
		                else {
		                    //Password incorretta. Registra il tentativo fallito nel database.
		                    $now = time();
		                    $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
		                    return false;
		                }
		            }
		        }
		        else {
		            return false; //L'utente inserito non esiste.
		        }
		    }
		}

		function checkbrute($user_id) {
				global $mysqli;
		    //Recupera il timestamp
		    $now = time();
		    //Analizza tutti i tentativi di login a partire dalle ultime due ore.
		    $valid_attempts = $now - (2 * 60 * 60);
		    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
		        $stmt->bind_param('i', $user_id);
		        $stmt->execute();
		        $stmt->store_result();
		        //Verifica l'esistenza di più di 5 tentativi di login falliti.
		        return ($stmt->num_rows > 5);
		    }
		}

		function login_check() {
				global $mysqli;
		    //Verifica che tutte le variabili di sessione siano impostate correttamente
		    if (isset($_SESSION['user_id'], $_SESSION['login_string'])) {
		        $user_id = $_SESSION['user_id'];
		        $login_string = $_SESSION['login_string'];
		        $user_browser = $_SERVER['HTTP_USER_AGENT']; //Reperisce la stringa 'user-agent' dell'utente.
		        if ($stmt = $mysqli->prepare("SELECT password FROM utenti WHERE id = ? LIMIT 1")) {
		            $stmt->bind_param('i', $user_id);
		            $stmt->execute();
		            $stmt->store_result();
		            if ($stmt->num_rows == 1) {
		                $stmt->bind_result($password);
		                $stmt->fetch();
		                $login_check = hash('sha512', $password . $user_browser);
		                if ($login_check == $login_string) {
		                    return true;
		                }
		            }
		        }
		    }
				return false;
		}

		function is_admin() {
				global $mysqli;
		    if (login_check()) {
			    	$stmt = $mysqli->prepare("SELECT admin FROM utenti WHERE id = ? LIMIT 1");
	          $stmt->bind_param('i', $_SESSION['user_id']);
	          $stmt->execute();
	          $stmt->store_result();
            $stmt->bind_result($isAdmin);
            $stmt->fetch();
            return $isAdmin;
		    }
				return false;
		}

	 	function random_string($length) {
		    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )), 1, $length);
		}

		function logout() {
			//Elimina tutti i valori della sessione.
	    $_SESSION = array();
	    //Recupera i parametri di sessione.
	    $params = session_get_cookie_params();
	    //Cancella i cookie attuali.
	    setcookie(session_name() , '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	    //Cancella la sessione.
	    session_destroy();
	    header('Location: ../index.php');
		}
?>
