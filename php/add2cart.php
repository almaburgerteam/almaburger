<?php
    require "functions.php";
    sec_session_start();

    if(!login_check()) {
      echo "NOLOGIN";
      die();
    }

    if(is_admin() || !isset($_POST["idProd"]) || !isset($_POST["qta"])) {
      echo "ERROR";
      die();
    }

    //Verifico se il prodotto è già presente nel carrello
    $stmt = $mysqli->prepare("SELECT quantita FROM carrelli WHERE idUtente = ? AND idProdotto = ?");
    $stmt->bind_param('dd', $_SESSION['user_id'], $_POST["idProd"]);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == 1) {
      //Se è già presente incremento la quantità
      $stmt->bind_result($qta);
      $stmt->fetch();
      $qta += $_POST["qta"];
      if($qta > 40) {
        echo "MAX";
        die();
      }
      $stmt = $mysqli->prepare("UPDATE carrelli SET quantita=?  WHERE idUtente=? AND idProdotto=?");
      $stmt->bind_param('ddd', $qta, $_SESSION['user_id'], $_POST["idProd"]);
      $stmt->execute();
    } else {
      //Altrimenti aggiungo il prodotto nel carrello
      $stmt = $mysqli->prepare("INSERT INTO carrelli(idUtente, idProdotto, quantita) VALUES (?, ?, ?)");
      $stmt->bind_param('ddd', $_SESSION['user_id'], $_POST["idProd"], $_POST["qta"]);
      $stmt->execute();
    }

    echo "OK";
?>
