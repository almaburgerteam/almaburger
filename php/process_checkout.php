<?php
  require "functions.php";
  sec_session_start();

  if(!login_check() || is_admin() || !isset($_POST["idSpedizione"]) || !isset($_POST["metodo"])) {
    header('Location: ../index.php');
    die();
  }

  //Recupero indirizzo di spedizione
  $stmt = $mysqli->prepare("SELECT cognome, nome, indirizzo, comune, provincia, cap FROM indirizzi WHERE id = ? AND idUtente = ?");
  $stmt->bind_param('dd', $_POST['idSpedizione'], $_SESSION['user_id']);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($cognome, $nome, $indirizzo, $comune, $provincia, $cap);
  if($stmt->num_rows <= 0) {
    header('Location: ../index.php');
    die();
  }
  $stmt->fetch();
  //Creo ordine
  $data = date('Y-m-d H:i:s');
  $stato = "IN ELABORAZIONE";
  $stmt = $mysqli->prepare("INSERT INTO ordini(idUtente, cognome, nome, indirizzo, comune, provincia, cap, data, metodoPagamento, stato) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
  $stmt->bind_param('dsssssssss', $_SESSION['user_id'], $cognome, $nome, $indirizzo, $comune, $provincia, $cap, $data, $_POST["metodo"], $stato);
  $stmt->execute();
  $idOrdine = $stmt->insert_id;
  //Creo dettaglio ordine
  $stmt = $mysqli->prepare("SELECT idProdotto, quantita, prezzo FROM carrelli INNER JOIN menu ON menu.id = carrelli.idProdotto WHERE idUtente = ?");
  $stmt->bind_param('d', $_SESSION['user_id']);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($idProdotto, $quantita, $prezzo);
  while($stmt->fetch()) {
    $stmt2 = $mysqli->prepare("INSERT INTO dettaglio_ordini(idOrdine, idProdotto, quantita, prezzo) VALUES (?, ?, ?, ?)");
    $stmt2->bind_param('dddd', $idOrdine, $idProdotto, $quantita, $prezzo);
    $stmt2->execute();
  }
  //Rimuovo il contenuto del carrello
  $stmt = $mysqli->prepare("DELETE FROM carrelli WHERE idUtente = ?");
  $stmt->bind_param('d', $_SESSION['user_id']);
  $stmt->execute();
  //Invio la notifica agli admin
  $stmt = $mysqli->prepare("SELECT id, email FROM utenti WHERE admin = 1");
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($idAdmin, $email);
  while($stmt->fetch()) {
    $to = $email;
    $subject = "Almaburger - Nuova ordinazione";
    $notifica = "Hai ricevuto una nuova ordinazione! Puoi visualizzare il dettaglio dell'ordine alla pagina: <a href='https://almaburger.altervista.org/order.php?id=$idOrdine'>https://almaburger.altervista.org/order.php?id=$idOrdine</a>";
    $body = "<p>$notifica</p>";
    $headers = "From: noreply@almaburger.com\r\n";
    $headers .= "Reply-To: noreply@almaburger.com\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "Content-Transfer-Encoding: 8bit\r\n";
    mail($to, $subject, $body, $headers);
    $stmt2 = $mysqli->prepare("INSERT INTO notifiche(idUtente, data, testo) VALUES (?, ?, ?)");
    $stmt2->bind_param('dss', $idAdmin, $data, $notifica);
    $stmt2->execute();
  }

  header('Location: ../order.php?id='.$idOrdine."&new=1");
?>
