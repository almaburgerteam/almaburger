<?php
    require "functions.php";

    //Recupera la password criptata dal form di inserimento.
    $password = $_POST['pwd'];
    //Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()) , true));
    //Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password . $random_salt);
    //Token di attivazione per conferma mail
    $token = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()) , true));
    $storedToken = hash('sha512', $token);

    $stmt = $mysqli->prepare("SELECT email FROM utenti WHERE email = ? LIMIT 1");
    $stmt->bind_param('s', $_POST['email']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->fetch();
    if($stmt->num_rows == 1) {
      header('Location: ../register.php?error=1');
    } else {
        $stmt = $mysqli->prepare("INSERT INTO utenti (nome, cognome, email, password, salt, telefono, token) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssssss', $_POST['nome'], $_POST['cognome'], $_POST['email'], $password, $random_salt, $_POST['telefono'], $storedToken);
        $stmt->execute();

        $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE email = ? LIMIT 1");
        $stmt->bind_param('s', $_POST['email']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($idUtente);
        $stmt->fetch();

        $stmt = $mysqli->prepare("INSERT INTO indirizzi (idUtente, cognome, nome, indirizzo, comune, provincia, cap) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('dssssss', $idUtente, $_POST['cognome'], $_POST['nome'], $_POST['indirizzo'], $_POST['comune'], $_POST['provincia'], $_POST['cap']);
        $stmt->execute();

        $to = $_POST['email'];
        $subject = "Almaburger - Attivazione account";
        $body = "<p>Per completare la registrazione e attivare il tuo account clicca sul seguente link: <a href='https://almaburger.altervista.org/activate.php?key=$token'>http://almaburger.altervista.org/activate.php?key=$token</a></p>";
        $headers = "From: noreply@almaburger.com\r\n";
        $headers .= "Reply-To: noreply@almaburger.com\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";
        $headers .= "Content-Transfer-Encoding: 8bit\r\n";
        mail($to, $subject, $body, $headers);

        header('Location: ../login.php?new=1');
    }
?>
