<?php
    require "functions.php";
    sec_session_start();

    if(!is_admin() || !isset($_POST["nome"]) || !isset($_POST["descrizione"]) || !isset($_POST["prezzo"])) {
      header("location: ../index.php");
      die();
    }

    //Controllo che nel menù non sia già presente un panino con lo stesso nome
    $stmt = $mysqli->prepare("SELECT id FROM menu WHERE nome = ? AND disponibile = 1");
    $stmt->bind_param('s', $_POST['nome']);
    $stmt->execute();
    $stmt->store_result();
    if(empty($_POST["id"]) && $stmt->num_rows > 0) {
      header("location: ../edit-burger.php?error=1");
      die();
    }

    //Controllo che il panino che voglio modificare esista
    if(!empty($_POST['id'])) {
      $stmt = $mysqli->prepare("SELECT immagine FROM menu WHERE id = ?");
      $stmt->bind_param('d', $_POST['id']);
      $stmt->execute();
      $stmt->store_result();
      $immagine = NULL;
      if($stmt->num_rows != 1) {
        header("location: ../edit-burger.php?error=2");
        die();
      } else {
        $stmt->bind_result($immagine);
        $stmt->fetch();
      }
    }

    if($_FILES["immagine"]["size"] > 0) {
      $targetDir = "../img/upload/";
      $extension = strtolower(pathinfo($_FILES["immagine"]["name"], PATHINFO_EXTENSION));
      $fileName = random_string(30) . "." . $extension;
      $path = $targetDir . $fileName;
      if (file_exists($path)) {
        header("location: ../edit-burger.php?error=1");
        die();
      }
      if ($_FILES["fileToUpload"]["size"] > 3145728) {
        header("location: ../edit-burger.php?error=3");
        die();
      }
      if(isset($immagine)) {
        //Se ho aggiornato l'immagine, cancello quella vecchia
        unlink("../img/upload/".$immagine);
        $immagine = $fileName;
      }
      if (!move_uploaded_file($_FILES["immagine"]["tmp_name"], $path)) {
        header("location: ../edit-burger.php?error=4");
        die();
      }
    }

    $piccante = isset($_POST["piccante"]);
    $glutine = isset($_POST["glutine"]);
    $vegetariano = isset($_POST["vegetariano"]);

    if(!empty($_POST["id"])) {
      //Aggiornamento prodotto esistente
      $stmt = $mysqli->prepare("UPDATE menu SET nome=?, descrizione=?, immagine=?, prezzo=?, piccante=?, senzaGlutine=?, vegetariano=? WHERE id = ?");
      $stmt->bind_param('sssddddd', $_POST['nome'], $_POST['descrizione'], $immagine, $_POST['prezzo'], $piccante, $glutine, $vegetariano, $_POST["id"]);
      $stmt->execute();
      header('Location: ../menu.php?edit=1');
    } else {
      //Nuovo prodotto
      $stmt = $mysqli->prepare("INSERT INTO menu (nome, descrizione, immagine, prezzo, piccante, senzaGlutine, vegetariano) VALUES (?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('sssdddd', $_POST['nome'], $_POST['descrizione'], $fileName, $_POST['prezzo'], $piccante, $glutine, $vegetariano);
      $stmt->execute();
      header('Location: ../menu.php?new=1');
    }
?>
