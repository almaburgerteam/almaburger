<?php
    require "functions.php";
    sec_session_start();

    if(!login_check() || !isset($_POST["idUtente"]) || !isset($_POST["nome"]) || !isset($_POST["cognome"]) || !isset($_POST["email"])) {
      header("location: ../index.php");
      die();
    }

    //Controllo che non sia già presente un utente con la stessa mail
    $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE email = ? AND id != ?");
    $stmt->bind_param('sd', $_POST['email'], $_POST['idUtente']);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 0) {
      header("location: ../profile.php?error=1");
      die();
    }

    //Controllo che l'utente che voglio modificare esista
    if(!empty($_POST['idUtente'])) {
      $stmt = $mysqli->prepare("SELECT immagine FROM utenti WHERE id = ?");
      $stmt->bind_param('d', $_POST['idUtente']);
      $stmt->execute();
      $stmt->store_result();
      $immagine = NULL;
      if($stmt->num_rows != 1) {
        header("location: ../profile.php?error=2");
        die();
      } else {
        $stmt->bind_result($immagine);
        $stmt->fetch();
      }
    }

    if($_FILES["immagine"]["size"] > 0) {
      $targetDir = "../img/upload/";
      $extension = strtolower(pathinfo($_FILES["immagine"]["name"], PATHINFO_EXTENSION));
      $fileName = random_string(30) . "." . $extension;
      $path = $targetDir . $fileName;
      if (file_exists($path)) {
        header("location: ../profile.php?error=3");
        die();
      }
      if ($_FILES["fileToUpload"]["size"] > 3145728) {
        header("location: ../profile.php?error=4");
        die();
      }
      if(isset($immagine)) {
        //Se ho aggiornato l'immagine, cancello quella vecchia
        if($immagine != "default.jpg") {
          unlink("../img/upload/".$immagine);
        }
        $immagine = $fileName;
      }
      if (!move_uploaded_file($_FILES["immagine"]["tmp_name"], $path)) {
        header("location: ../edit-burger.php?error=5");
        die();
      }
    }

    //Aggiornamento utente
    $stmt = $mysqli->prepare("UPDATE utenti SET nome=?, cognome=?, email=?, telefono=?, codFisc=?, immagine=? WHERE id = ?");
    $stmt->bind_param('ssssssd', $_POST['nome'], $_POST['cognome'], $_POST['email'], $_POST['telefono'], $_POST['codFisc'], $immagine, $_POST["idUtente"]);
    $stmt->execute();
    header('Location: ../profile.php?success=1');
?>
