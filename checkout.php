<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
  }
  if(is_admin()) {
    header('Location: index.php');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <style>
      .table > tbody > tr > td {
      vertical-align: middle;
      border: 0px;
      }
    </style>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Checkout ordine</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <h1 class="mb-4">Checkout ordine</h1>
        <h2>Riepilogo prodotti</h2>
        <table class="table table-striped">
          <thead>
            <tr class="text-center">
              <th class="d-none d-sm-table-cell"></th>
              <th>Prodotto</th>
              <th>Prezzo</th>
              <th>Quantità</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $totale = 0;
              $stmt = $mysqli->prepare("SELECT id, nome, immagine, prezzo, quantita FROM carrelli INNER JOIN menu ON menu.id = carrelli.idProdotto WHERE idUtente = ?");
              $stmt->bind_param("i", $_SESSION['user_id']);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($id, $nome, $immagine, $prezzo, $quantita);
              if($stmt->num_rows <= 0) {
                header('Location: cart.php');
                die();
              } else {
                while($stmt->fetch()) {
                  $parziale = $prezzo * $quantita;
                  $totale += $parziale; ?>
            <tr class="text-center align-middle">
              <td class="d-none d-sm-table-cell">
                <div class="row">
                  <div class="col-md-12"><img class="w-100" style="max-width:180px;" src="img/upload/<?php echo $immagine; ?>" alt="<?php echo $nome; ?>"></div>
                </div>
              </td>
              <td data-th="Prodotto"><?php echo $nome; ?></td>
              <td data-th="Prezzo"><?php echo $prezzo; ?>€</td>
              <td data-th="Quantità"><?php echo $quantita; ?></td>
            </tr>
            <?php
              }
              }
              ?>
          </tbody>
        </table>
        <h2 class="mb-4">Totale <?php echo $totale; ?>€</h2>
        <hr class="mb-4">
        <h2>Indirizzo di spedizione</h2>
        <select id="spedizione" class="form-control mb-4">
          <?php
            $stmt = $mysqli->prepare("SELECT id, cognome, nome, indirizzo, comune, provincia, cap FROM indirizzi WHERE idUtente = ?");
            $stmt->bind_param("i", $_SESSION['user_id']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $cognome, $nome, $indirizzo, $comune, $provincia, $cap);
            while($stmt->fetch()) { ?>
          <option value="<?php echo $id; ?>"><?php echo $cognome." ".$nome." - ".$indirizzo." - ".$comune.", ".$provincia." ".$cap; ?></option>
          <?php
            }
            ?>
        </select>
        <hr class="mb-4">
        <h2>Metodo di pagamento</h2>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a href="#cartacredito" class="nav-link active" aria-controls="cartacredito" aria-selected="true" data-toggle="tab" id="cartacredito-tab" role="tab">Carta di credito</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#paypal" aria-controls="paypal" aria-selected="false" data-toggle="tab" id="paypal-tab" role="tab">Paypal</a>
          </li>
          <li class="nav-item">
            <a href="#bitcoin" class="nav-link" aria-controls="bitcoin" aria-selected="false" data-toggle="tab" id="bitcoin-tab" role="tab">Bitcoin</a>
          </li>
        </ul>
        <div class="tab-content my-2">
          <div class="tab-pane fade show active" id="cartacredito" role="tabpanel">
            <form action="php/process_checkout.php" id="frmCarta" method="post">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label for="carta">Numero carta di credito</label>
                  <input type="text" id="carta" name="carta" minlength="16" maxlength="16" required placeholder="Inserisci il numero della carta" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                  <label for="intestatario">Intestatario</label>
                  <input type="text" id="intestatario" name="intestatario" required placeholder="Inserisci nome e cognome" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label for="scadenza">Data di scadenza</label>
                  <input type="text" id="scadenza" name="scadenza" minlength="5" maxlength="5" required placeholder="MM/AA" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                  <label for="cvv">Codice CVV</label>
                  <input type="text" id="cvv" name="cvv" minlength="3" maxlength="3" required placeholder="Inserisci il codice CVV" class="form-control">
                </div>
              </div>
              <div class="text-center">
                <input type="text" class="spedizione" name="idSpedizione" required hidden>
                <button name="metodo" value="carta" class="col-md-6 btn btn-success btn-lg">Paga Ora</button>
              </div>
            </form>
          </div>
          <div class="tab-pane fade text-center" id="paypal" role="tabpanel">
            <form action="php/process_checkout.php" method="post">
              <input type="text" class="spedizione" name="idSpedizione" required hidden>
              <input type="image" name="metodo" value="paypal" class="col-md-6" src="img/res/paypal.png" alt="Paga con paypal"/>
            </form>
          </div>
          <div class="tab-pane fade text-center" id="bitcoin" role="tabpanel">
            <form action="php/process_checkout.php" method="post">
              <input type="text" class="spedizione" name="idSpedizione" required hidden>
              <img class="col-md-4 mb-2" src='img/res/bitcoin.png' alt="Bitcoin address"><br>
              <button name="metodo" value="bitcoin" class="col-md-6 btn btn-success btn-lg">Paga Ora</button>
            </form>
          </div>
        </div>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $(document).ready(function() {
        var id = $("select#spedizione").val();
        $("input.spedizione").val(id);
      });
      $("#spedizione").change(function() {
        var id = $("select#spedizione").val();
        $("input.spedizione").val(id);
      });

      $.validator.addMethod('check_names', function (value) {
          return /^[A-Za-z ]+$/.test(value);
      }, 'Inserisci un nome valido');

      $.validator.addMethod('check_scadenza', function (value) {
          return /^[0-9]{2}[/][0-9]{2}/.test(value);
      }, 'Inserisci una data di scadenza valida nel formato richiesto');

      $("#frmCarta").validate({
         rules: {
            intestatario: {
              check_names: true
            },
            carta: {
              creditcard: true
            },
            scadenza: {
              check_scadenza: true
            },
            cvv: {
              number: true
            }
         }
       });
    </script>
  </body>
</html>
