<?php
  require 'php/functions.php';
  sec_session_start();
  if (login_check()) {
    header('Location: index.php');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Registrati</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['error'])) {
              echo '<p class="text-center alert alert-danger">Registrazione fallita: è già presente un account associato a questo indirizzo email.</p><br>';
          }
          ?>
        <fieldset class= "border border-light mt-2">
          <legend  class="w-50 text-center">Registrati ad Almaburger</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" method="post" action="php/process_register.php">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" placeholder="Inserisci il tuo nome" class="form-control" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="cognome">Cognome</label>
                    <input type="text" name="cognome" id="cognome" placeholder="Inserisci il tuo cognome" class="form-control" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="email" placeholder="Inserisci la tua email" class="form-control" required>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="pwd">Password</label>
                    <input type="password" name="pwd" id="pwd" minlength="6" placeholder="******" class="form-control" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="conf_pwd">Ripeti password</label>
                    <input type="password" name="conf_pwd" id="conf_pwd" minlength="6" placeholder="******" class="form-control" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="indirizzo">Indirizzo</label>
                  <input type="text" name="indirizzo" id="indirizzo" placeholder="Inserisci il tuo indirizzo" class="form-control" required>
                </div>
                <div class="row">
                  <div class="col-sm-4 form-group">
                    <label for="comune">Comune</label>
                    <input type="text" name="comune" id="comune" placeholder="Inserisci il tuo comune" class="form-control" required>
                  </div>
                  <div class="col-sm-4 form-group">
                    <label for="provincia">Provincia</label>
                    <input type="text" name="provincia" minlength="2" maxlength="2" id="provincia" placeholder="Inserisci la tua provincia" class="form-control" required>
                  </div>
                  <div class="col-sm-4 form-group">
                    <label for="cap">CAP</label>
                    <input type="text" name="cap" id="cap" minlength="5" maxlength="5" placeholder="Inserisci il tuo CAP" class="form-control" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="telefono">Telefono</label>
                  <input type="tel" name="telefono" id="telefono" minlength="10" maxlength="10" placeholder="Inserisci il tuo telefono" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Registrati</button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $.validator.addMethod('check_telephone', function (value) {
        if(value != "") {
          return /^\d{10}$/.test(value);
        } else return true;
      }, 'Inserisci un numero di telefono valido');

      $.validator.addMethod('check_email', function (value) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      }, 'Inserisci un indirizzo email valido');

      $.validator.addMethod('check_names', function (value) {
          return /^[A-Za-z ]+$/.test(value);
      }, 'Inserisci un nome valido');

      $.validator.addMethod('check_cap', function (value) {
          return /^\d{5}$/.test(value);
      }, 'Inserisci un CAP valido');

      $("#form").validate({
         rules: {
            conf_pwd: {
                equalTo: "#pwd"
            },
            telefono: {
                check_telephone: true
            },
            email: {
                check_email: true
            },
            nome: {
                check_names: true
            },
            cognome: {
                check_names: true
            },
            cap: {
                check_cap: true
            }
         }
       });
    </script>
  </body>
</html>
