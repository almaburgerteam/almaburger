<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
    die();
  }
  if (!is_admin()) {
    header('Location: index.php');
    die();
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Elenco ordini</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <h2>Elenco ordini</h2>
        <table class="table table-striped" style="margin-left:-7px; margin-bottom:0px;">
          <thead>
            <tr class="text-center">
              <th>N. Ordine</th>
              <th>Data</th>
              <th>Totale</th>
              <th>Stato</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $stmt = $mysqli->prepare("SELECT id, data, stato, SUM(prezzo * quantita) AS totale FROM ordini INNER JOIN dettaglio_ordini ON ordini.id = dettaglio_ordini.idOrdine GROUP BY id ORDER BY id DESC LIMIT 100");
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($id, $data, $stato, $totale);
              while($stmt->fetch()) {
                $date = new DateTime($data);
                $dataFormattata = $date->format('d/m/Y H:i');?>
            <tr class="text-center align-middle">
              <td data-th="N. Ordine"><a href="order.php?id=<?php echo $id; ?>">#<?php echo $id; ?></a></td>
              <td data-th="Data"><?php echo $dataFormattata; ?></td>
              <td data-th="Totale"><?php echo $totale; ?>€</td>
              <td data-th="Stato"><?php echo $stato; ?></td>
            </tr>
            <?php
              }
              ?>
          </tbody>
        </table>
      </div>
    </main>
    <?php require("footer.php"); ?>
  </body>
</html>
