<nav class="navbar-expand-md navbar-dark bg-dark navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <a class="navbar-brand" href="index.php"><img src="img/res/almalogo3.png" alt="logo"></a>
    <button type="button" style="margin-top:10px;" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbar-menu" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
    <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse text-center" style="margin-top:10px;" id="navbar-menu">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link" href="menu.php">Menù</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="about-us.php">Chi siamo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contact.php">Contattaci</a>
        </li>
      </ul>
      <a href="profile.php" class="btn btn-default navbar-btn btn-lg"><em class="fa fa-fw fa-user-circle-o"></em></a>
      <?php
        if(is_admin()) {
          $status = "IN ELABORAZIONE";
          $stmt = $mysqli->prepare("SELECT COUNT(*) AS numero FROM ordini WHERE stato = ?");
          $stmt->bind_param('s', $status);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($numeroOrdini);
          $stmt->fetch();
          $lightOn = ($numeroOrdini > 0) ? "text-light": "";
          if($numeroOrdini == 0) $numeroOrdini = "";
          echo '<a href="orderlist.php" class="btn btn-default navbar-btn btn-lg"><span class="'.$lightOn.'"><em class="fa fa-fw fa-list-ul"></em>'.$numeroOrdini.'</span></a>';
        } else {
          if(login_check()) {
            $stmt = $mysqli->prepare("SELECT SUM(quantita) AS numero FROM carrelli WHERE idUtente = ?");
            $stmt->bind_param('i', $_SESSION['user_id']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($numeroProdotti);
            $stmt->fetch();
            $lightOn = ($numeroProdotti > 0) ? "text-light": "";
            if($numeroProdotti == 0) $numeroProdotti = "";
          }
          echo '<a href="cart.php" class="btn btn-default navbar-btn btn-lg"><span class="'.$lightOn.'"><em class="fa fa-fw fa-shopping-cart"></em>'.$numeroProdotti.'</span></a>';
        }
        ?>
      <?php
        if(login_check()) {
          $stmt = $mysqli->prepare("SELECT COUNT(*) AS numero FROM notifiche WHERE idUtente = ? AND letta = 0");
          $stmt->bind_param('i', $_SESSION['user_id']);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($numeroNotifiche);
          $stmt->fetch();
          $lightOn = ($numeroNotifiche > 0) ? "text-light": "";
          if($numeroNotifiche == 0) $numeroNotifiche = "";
        }
        ?>
      <a href="notifications.php" class="btn btn-default navbar-btn btn-lg"><span class="<?php echo $lightOn; ?>"><em class="fa fa-fw fa-bell-o"></em><?php echo $numeroNotifiche; ?></span></a>
      <form method="get" class="mx-auto" style="max-width:240px;" action="menu.php">
        <div class="input-group">
          <input id="search" type="text" class="form-control" name="search" placeholder="Cerca...">
          <span class="input-group-btn">
          <button class="btn btn-secondary" style="border-radius:0px;" type="submit"><em class="fa fa-fw fa-search"></em></button>
          </span>
        </div>
      </form>
    </div>
  </div>
</nav>
