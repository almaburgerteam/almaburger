<?php
  require 'php/functions.php';
  sec_session_start();
  if (login_check()) {
    header('Location: index.php');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Login</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['error']) && $_GET['error'] == "invalid") {
              echo '<p class="text-center alert alert-danger">Autenticazione fallita.<br><br><a class="btn btn-danger" href="forgot-password.php">Password dimenticata?</a></p><br>';
          } else if(isset($_GET['error']) && $_GET['error'] == "required") {
              echo '<p class="text-center alert alert-info">Per eseguire questa operazione devi prima fare il login!</p><br>';
          } else if(isset($_GET['new']) && $_GET['new'] == 1) {
              echo '<p class="text-center alert alert-success">Registrazione avvenuta con successo! Controlla le tue mail per attivare il tuo account</p><br>';
          } else if(isset($_GET['new']) && $_GET['new'] == 2) {
              echo '<p class="text-center alert alert-success">Account attivato! Ora puoi effettuare il login</p><br>';
          } else if(isset($_GET['reset']) && $_GET['reset'] == 1) {
              echo "<p class='text-center alert alert-success'>Ti abbiamo inviato una mail contenente un link per reimpostare la password</p><br>";
          } else if(isset($_GET['reset']) && $_GET['reset'] == 2) {
              echo "<p class='text-center alert alert-success'>Password impostata correttamente</p><br>";
          }
          ?>
        <fieldset class= "border border-light mt-2">
          <legend  class="w-50 text-center">Effettua il login</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" action="php/process_login.php" method="post">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="email" placeholder="Inserisci la tua email" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="pwd">Password</label>
                  <input type="password" name="pwd" id="pwd" minlength="6" placeholder="******" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
              </form>
            </div>
          </div>
        </fieldset>
        <br>
        <hr>
        <br>
        <div class="container text-center">
          <h2>Sei un nuovo cliente?</h2>
          <a href="register.php" class="btn btn-info" role="button">Registrati</a>
        </div>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $.validator.addMethod('check_email', function (value) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      }, 'Inserisci un indirizzo email valido');

      $("#form").validate({
        rules: {
          email: {
            check_email: true
          }
       }
      });
    </script>
  </body>
</html>
