<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
    die();
  }
  $stmt = $mysqli->prepare("SELECT idUtente, cognome, nome, indirizzo, comune, provincia, cap, data, metodoPagamento, stato, recensione FROM ordini WHERE id = ?");
  $stmt->bind_param('d', $_GET['id']);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($idUtente, $cognome, $nome, $indirizzo, $comune, $provincia, $cap, $data, $metodoPagamento, $stato, $recensione);
  if($stmt->num_rows <= 0) {
    header('Location: index.php');
    die();
  }
  $stmt->fetch();
  if(!is_admin() && $_SESSION['user_id'] != $idUtente) {
    header('Location: index.php');
    die();
  }

  function metodoDiPagamento($metodo) {
    switch ($metodo) {
      case 'bitcoin':
        return "Bitcoin";
      case 'carta':
        return "Carta di credito";
      case 'paypal':
        return "Paypal";
      default:
        return "Sconosciuto";
    }
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <style>
      .table > tbody > tr > td {
      vertical-align: middle;
      border: 0px;
      }
    </style>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Dettaglio ordine</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['new'])) {
              echo '<p class="text-center alert alert-success">Ordine inoltrato correttamente!</p><br>';
          }
          ?>
        <div class="row">
          <div class="col-md-12">
            <h1>Ordine #<?php echo $_GET['id']; ?></h1>
          </div>
        </div>
        <div class="row mb-2">
          <div class="col-md-12">
            <?php
              $date = new DateTime($data);
              $dataFormattata = $date->format('d/m/Y H:i:s');
              ?>
            <p>Data: <?php echo $dataFormattata; ?></p>
            <p>Metodo di pagamento: <?php echo metodoDiPagamento($metodoPagamento); ?></p>
          </div>
        </div>
        <div class="row mb-4">
          <div class="col-md-6 my-auto">
            <h2 class="mb-0">Stato: <?php echo $stato; ?></h2>
          </div>
          <div class="col-md-6">
            <?php
              if(is_admin() && $stato == "IN ELABORAZIONE") { ?>
            <form action="php/update_order.php" method="post">
              <input type="text" name="idOrdine" value="<?php echo $_GET['id']; ?>" hidden>
              <button type="submit" class="btn btn-success btn-block" name="azione" value="spedito">Evadi ordine</button>
            </form>
            <?php
              } else if(is_admin() && $stato == "IN CONSEGNA") { ?>
            <form action="php/update_order.php" method="post">
              <input type="text" name="idOrdine" value="<?php echo $_GET['id']; ?>" hidden>
              <button type="submit" class="btn btn-success btn-block" name="azione" value="consegnato">Ordine consegnato</button>
            </form>
            <?php
              }
              ?>
          </div>
        </div>
        <div class="row mb-4">
          <div class="col-md-5">
            <div class="card">
              <div class="card-header bg-primary">
                <h5 class="mb-0 text-center">Indirizzo di spedizione</h5>
              </div>
              <div class="card-body text-center">
                <div class="row m-1"><?php echo $nome." ".$cognome; ?></div>
                <div class="row m-1"><?php echo $indirizzo; ?></div>
                <div class="row m-1"><?php echo $comune.", ".$provincia." ".$cap; ?></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h2>Riepilogo ordine</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped">
              <thead>
                <tr class="text-center">
                  <th class="d-none d-sm-table-cell"></th>
                  <th>Prodotto</th>
                  <th>Prezzo</th>
                  <th>Quantità</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $totale = 0;
                  $stmt = $mysqli->prepare("SELECT nome, immagine, dettaglio_ordini.prezzo, quantita FROM dettaglio_ordini INNER JOIN menu ON menu.id = dettaglio_ordini.idProdotto WHERE idOrdine = ?");
                  $stmt->bind_param("d", $_GET['id']);
                  $stmt->execute();
                  $stmt->store_result();
                  $stmt->bind_result($nome, $immagine, $prezzo, $quantita);
                    while($stmt->fetch()) {
                      $parziale = $prezzo * $quantita;
                      $totale += $parziale; ?>
                <tr class="text-center align-middle">
                  <td class="d-none d-sm-table-cell">
                    <div class="row">
                      <div class="col-md-12"><img class="w-100" style="max-width:180px;" src="img/upload/<?php echo $immagine; ?>" alt="<?php echo $nome; ?>"></div>
                    </div>
                  </td>
                  <td data-th="Prodotto"><?php echo $nome; ?></td>
                  <td data-th="Prezzo"><?php echo $prezzo; ?>€</td>
                  <td data-th="Quantità"><?php echo $quantita; ?></td>
                </tr>
                <?php
                  }
                  ?>
              </tbody>
            </table>
            <h2 class="mb-0 my-auto">Totale: <?php echo $totale; ?>€</h2>
          </div>
        </div>
        <?php
          if($stato=="CONSEGNATO" && $idUtente == $_SESSION['user_id']) { ?>
        <fieldset class= "border border-light mt-4">
          <legend  class="w-50 text-center">Scrivi una recensione!</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <div class="col-md-12">
                <?php
                  if(empty($recensione)) { ?>
                <form class="" action="php/publish_review.php" method="post">
                  <div class="form-group">
                    <label for="recensione">Recensione</label>
                    <textarea class="form-control" id="recensione" rows="5" maxlength="300" style="resize:none;" name="recensione" placeholder="La tua recensione" required></textarea>
                  </div>
                  <input type="text" name="idOrdine" value="<?php echo $_GET['id']; ?>" hidden>
                  <button type="submit" class="btn btn-primary btn-lg btn-block">Invia recensione</button>
                </form>
                <?php
                  } else { ?>
                <textarea class="form-control" rows="5" style="resize:none;" readonly><?php echo $recensione; ?></textarea>
                <?php
                  }
                  ?>
              </div>
            </div>
          </div>
        </fieldset>
        <?php
          }
          ?>
      </div>
    </main>
    <?php require("footer.php"); ?>
  </body>
</html>
