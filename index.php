<?php
  require 'php/functions.php';
  sec_session_start();
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/animate-in.js"></script>
    <script src="js/smooth-scroll.js"></script>
    <title>Home</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main>
      <div class="py-3 text-white bg-secondary">
        <div class="container">
          <div class="row">
            <div class="align-self-center col-md-6 ">
              <h1>Almaburger</h1>
              <br>
              <p><em>Mangiare è uno dei quattro scopi della vita… quali siano gli altri tre, nessuno lo ha mai saputo.<br>(Carlo Cracco)</em></p>
              <p><em>Ho dei gusti semplicissimi; mi accontento sempre del meglio.<br>(Oscar Wilde)</em></p>
            </div>
            <div class="col-md-6 p-0">
              <div id="carousel1" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img src="img/res/burgermania-gallery-01.jpg" alt="locale 1" class="d-block img-fluid w-100">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid w-100" alt="locale 2" src="img/res/burgermania-gallery-02.jpg" data-holder-rendered="true">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid w-100" alt="locale 3" src="img/res/burgermania-gallery-05.jpg" data-holder-rendered="true">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="py-3 text-white bg-secondary photo-overlay" style="background-image: url(img/res/sfondo_menu.jpg);">
        <div class="container bg-dark">
          <div class="row animate-in-down">
            <div class="col-md-6 p-0 bg-secondary">
              <div id="carousel2" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner bg-secondary " role="listbox">
                  <?php
                    $imgFolder="img/upload/";
                    $count=1;
                    $stmt = $mysqli->prepare("SELECT nome, descrizione, immagine FROM menu");
                    $stmt->execute();
                    $stmt->store_result();
                    if($stmt->num_rows > 0) {
                        $stmt->bind_result($nome, $descrizione, $immagine);
                        while($stmt->fetch()) { ?>
                  <div class="carousel-item <?php if($count==1) echo "active"; ?>">
                    <img class="d-block img-fluid w-100" style="height:320px;" src="<?php echo $imgFolder.$immagine ?>" alt="<?php echo $nome; ?>">
                    <p class="my-3 text-center text-light" style="padding:0px 5px; min-height:80px;"><?php echo $nome."<br>".$descrizione; ?></p>
                  </div>
                  <?php
                    $count++;
                    }
                    }
                    ?>
                </div>
                <a class="carousel-control-prev" href="#carousel2" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                <a class="carousel-control-next" href="#carousel2" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
              </div>
            </div>
            <div class="align-self-center p-4 col-md-6 bg-dark animate-in-down">
              <h1 class="mb-4 text-center"><em>I NOSTRI PANINI</em></h1>
              <p class="mb-5">I nostri hamburger sono preparati ogni giorno nei nostri locali con ingredienti freschi della zona. Puoi gustarli da noi, portarteli a casa o addirittura farteli consegnare a domicilio!</p>
              <a class="btn btn-outline-light btn-block btn-lg" href="menu.php" data-toggle="">Scopri i nostri panini</a>
            </div>
          </div>
        </div>
      </div>
      <div class="py-3 text-white text-center photo-overlay" style="background-image: url(img/res/sfondo_recensione1.jpg);">
        <div class="container">
          <div class="row justify-content-center animate-in-down">
            <div class="col-md-8">
              <div class="carousel slide" data-ride="carousel" id="carouselArchitecture">
                <div class="carousel-inner bg-dark rounded" role="listbox">
                  <?php
                    $counter = 1;
                    $stmt = $mysqli->prepare("SELECT recensione, immagine FROM ordini INNER JOIN utenti ON ordini.idUtente = utenti.id WHERE recensione <> '' ORDER BY ordini.id DESC LIMIT 5");
                    $stmt->execute();
                    $stmt->store_result();
                    $stmt->bind_result($recensione, $immagine);
                    while($stmt->fetch()) { ?>
                  <div class="carousel-item <?php if($counter == 1) echo "active"; ?>">
                    <div class="row justify-content-center mx-auto">
                      <div class="col-md-5 pt-3">
                        <div class="circle-avatar" style="background-image:url(img/upload/<?php echo $immagine; ?>)"></div>
                      </div>
                      <p class="mt-3 pl-1 pr-1" style="min-height:120px;"><?php echo $recensione; ?></p>
                    </div>
                  </div>
                  <?php
                    $counter++;
                    }
                    ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <?php require("footer.php"); ?>
  </body>
</html>
