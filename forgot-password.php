<?php
  require 'php/functions.php';
  sec_session_start();
  if (login_check()) {
    header('Location: index.php');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Password dimenticata</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <fieldset class= "border border-light mt-2">
          <legend  class="w-50 text-center">Password dimenticata</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" action="php/reset.php" method="post">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="email" placeholder="Inserisci la tua email" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Ripristina password</button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $.validator.addMethod('check_email', function (value) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      }, 'Inserisci un indirizzo email valido');

      $("#form").validate({
        rules: {
          email: {
            check_email: true
          }
       }
      });
    </script>
  </body>
</html>
