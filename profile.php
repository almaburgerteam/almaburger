<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check() && isset($_GET['reset'])) {
    header('Location: login.php?reset=1');
  } else if (!login_check()) {
    header('Location: login.php?error=required');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-notify.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Il tuo profilo</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['success']) && $_GET['success'] == 1) {
              echo '<p class="text-center alert alert-success">Modifica avvenuta con successo</p><br>';
          } else if(isset($_GET['success']) && $_GET['success'] == 2) {
              echo "<p class='text-center alert alert-success'>Indirizzo di spedizione aggiunto correttamente</p><br>";
          } else if(isset($_GET['success']) && $_GET['success'] == 3) {
              echo "<p class='text-center alert alert-success'>Indirizzo di spedizione aggiornato correttamente</p><br>";
          } else if(isset($_GET['success']) && $_GET['success'] == 4) {
              echo "<p class='text-center alert alert-success'>Indirizzo di spedizione rimosso correttamente</p><br>";
          } else if(isset($_GET['reset']) && $_GET['reset'] == 1) {
              echo "<p class='text-center alert alert-success'>Ti abbiamo inviato una mail contenente un link per reimpostare la password</p><br>";
          } else if(isset($_GET['error']) && $_GET['error'] == 1) {
              echo "<p class='text-center alert alert-danger'>Modifica fallita. E' già presente un altro utente registrato con questo indirizzo email.</p><br>";
          } else if(isset($_GET['error']) && $_GET['error'] == 7) {
              echo "<p class='text-center alert alert-danger'>Impossibile rimuovere l'indirizzo. E' necessario avere almeno un indirizzo di spedizione!</p><br>";
          } else if(isset($_GET['error'])) {
            echo '<p class="text-center alert alert-danger">Modifica fallita. Si è verificato un errore imprevisto...</p><br>';
          }
          $stmt = $mysqli->prepare("SELECT id, nome, cognome, email, telefono, codFisc, immagine FROM utenti WHERE id = ? LIMIT 1");
          $stmt->bind_param('s', $_SESSION['user_id']);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($id, $nome, $cognome, $email, $telefono, $codFisc, $immagine);
          $stmt->fetch();
          ?>
        <div class="row">
          <div class="col-md-9">
            <h1>Benvenuto <?php echo $nome." ".$cognome; ?>!</h1>
          </div>
          <div class="col-md-3 text-right">
            <a class="btn btn-danger" href="php/logout.php"><em class="fa fa-sign-out"></em> Effettua il logout</a>
          </div>
        </div>
        <fieldset class= "border border-light mt-4">
          <legend  class="w-50 text-center">Dati personali</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" method="post" action="php/edit_user.php" enctype="multipart/form-data">
                <div class="row justify-content-center">
                  <div class="col-sm-4 form-group">
                    <div class="circle-avatar" style="background-image:url(img/upload/<?php echo $immagine; ?>)"></div>
                    <div class="text-center mt-2">
                      <input type="file" id="immagine" class="form-control" name="immagine">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" value="<?php echo $nome; ?>" required placeholder="Inserisci il tuo nome" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="cognome">Cognome</label>
                    <input type="text" name="cognome" id="cognome" value="<?php echo $cognome; ?>" required placeholder="Inserisci il tuo cognome" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="email" value="<?php echo $email; ?>" required placeholder="Inserisci la tua email" class="form-control">
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" placeholder="******" class="form-control" readonly>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label style="visibility: hidden;">Password</label>
                    <a href="php/reset.php" class="btn btn-primary btn-block">Cambia password</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group ">
                    <label for="codFisc">Codice fiscale</label>
                    <input type="text" name="codFisc" id="codFisc" value="<?php echo $codFisc; ?>" minlength="16" maxlength="16" placeholder="Inserisci il tuo codice fiscale" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group ">
                    <label for="telefono">Telefono</label>
                    <input type="tel" name="telefono" id="telefono" value="<?php echo $telefono; ?>" minlength="10" maxlength="10" placeholder="Inserisci il tuo telefono" class="form-control">
                  </div>
                </div>
                <input type="number" name="idUtente" value="<?php echo $id; ?>" hidden>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Salva</button>
              </form>
            </div>
          </div>
        </fieldset>
        <fieldset class= "border border-light mt-5">
          <legend  class="w-50 text-center">Indirizzi di spedizione</legend>
          <div class="row justify-content-center mb-2">
            <?php
              $stmt = $mysqli->prepare("SELECT id, nome, cognome, indirizzo, comune, provincia, cap FROM indirizzi WHERE idUtente = ?");
              $stmt->bind_param('d', $_SESSION['user_id']);
              $stmt->execute();
              $stmt->store_result();
              if($stmt->num_rows > 0) {
                  $stmt->bind_result($id, $nome, $cognome, $indirizzo, $comune, $provincia, $cap);
                  $counter = 1;
                  while($stmt->fetch()) { ?>
            <div class="col-md-5 mt-2">
              <div class="card">
                <div class="card-header bg-primary">
                  <h5 class="mb-0 text-center">Indirizzo n. <?php echo $counter; ?></h5>
                </div>
                <div class="card-body text-center vcenter" style="max-height:110px;">
                  <div class="row m-1"><?php echo $nome." ".$cognome; ?></div>
                  <div class="row m-1"><?php echo $indirizzo; ?></div>
                  <div class="row m-1"><?php echo $comune.", ".$provincia." ".$cap; ?></div>
                </div>
                <div class="card-footer force-to-bottom text-center">
                  <form action="edit-address.php" method="post">
                    <input type="number" name="idIndirizzo" value="<?php echo $id; ?>" hidden>
                    <button type="submit" name="modifica" class="btn btn-warning fa fa-pencil-square-o"> Modifica</button>
                    <button type="submit" name="elimina" onclick="conferma(event)" class="btn btn-danger fa fa-trash-o"> Elimina</button>
                  </form>
                </div>
              </div>
            </div>
            <?php
              $counter++;
              }
              }
              ?>
            <div class="col-md-5 mt-2">
              <div class="card">
                <div class="card-header bg-primary">
                  <h5 class="mb-0 text-center">Aggiungi indirizzo</h5>
                </div>
                <div class="card-body text-center vcenter" style="height:110px;">
                  <img src="img/res/address.png" style="height:80px;" alt="indirizzo">
                </div>
                <div class="card-footer force-to-bottom text-center">
                  <form>
                    <button type="button" onclick="location.href='edit-address.php'" class="btn btn-success fa fa-plus"> Aggiungi</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </fieldset>
        <h2 class="mt-5">Storico ordini</h2>
        <table class="table table-striped" style="margin-left:-7px;">
          <thead>
            <tr class="text-center">
              <th>N. Ordine</th>
              <th>Data</th>
              <th>Totale</th>
              <th>Stato</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $stmt = $mysqli->prepare("SELECT id, data, stato, SUM(prezzo * quantita) AS totale FROM ordini INNER JOIN dettaglio_ordini ON ordini.id = dettaglio_ordini.idOrdine WHERE idUtente = ? GROUP BY id");
              $stmt->bind_param("i", $_SESSION['user_id']);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($id, $data, $stato, $totale);
              while($stmt->fetch()) {
                $date = new DateTime($data);
                $dataFormattata = $date->format('d/m/Y H:i');?>
            <tr class="text-center align-middle">
              <td data-th="N. Ordine"><a href="order.php?id=<?php echo $id; ?>">#<?php echo $id; ?></a></td>
              <td data-th="Data"><?php echo $dataFormattata; ?></td>
              <td data-th="Totale"><?php echo $totale; ?>€</td>
              <td data-th="Stato"><?php echo $stato; ?></td>
            </tr>
            <?php
              }
              ?>
          </tbody>
        </table>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      function conferma(e) {
        if(!confirm('Sicuro di voler eliminare questo indirizzo?'))e.preventDefault();
      }

      $('#immagine').on("change", function(){
        $.notify({
          message: "Per visualizzare la nuova immagine salva le modifiche!"
        },{
          type: "success",
          offset: {
            x: 0,
            y: 10
          }
        });
      });

      $.validator.addMethod('check_telephone', function (value) {
        if(value != "") {
          return /^\d{10}$/.test(value);
        } else return true;
      }, 'Inserisci un numero di telefono valido');

      $.validator.addMethod('check_email', function (value) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      }, 'Inserisci un indirizzo email valido');

      $.validator.addMethod('check_names', function (value) {
          return /^[A-Za-z ]+$/.test(value);
      }, 'Inserisci un nome valido');

      $.validator.addMethod('check_codicefiscale', function (cf) {
        cf = cf.toUpperCase();
        if( cf == '' )  return true;
        if( ! /^[0-9A-Z]{16}$/.test(cf) )
          return false;
        var map = [1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17,
          19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23];
        var s = 0;
        for(var i = 0; i < 15; i++){
          var c = cf.charCodeAt(i);
          if( c < 65 )
            c = c - 48;
          else
            c = c - 55;
          if( i % 2 == 0 )
            s += map[c];
          else
            s += c < 10? c : c - 10;
        }
        var atteso = String.fromCharCode(65 + s % 26);
        if( atteso != cf.charAt(15) )
          return false;
        return true;
       }, "Inserisci un codice fiscale valido");

      $("#form").validate({
         rules: {
            telefono: {
                check_telephone: true
            },
            email: {
                check_email: true
            },
            nome: {
                check_names: true
            },
            cognome: {
                check_names: true
            },
            codFisc: {
                check_codicefiscale: true
            },
            immagine: {
              accept: "image/*"
            }
         },
         messages: {
           immagine: "Carica un'immagine valida"
         }
       });
    </script>
  </body>
</html>
