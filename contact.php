<?php
  require 'php/functions.php';
  sec_session_start();

  if(isset($_POST["nome"]) && isset($_POST["email"]) && isset($_POST["messaggio"])) {
    $to = "matteo.saccomanni@icloud.com";
    $subject = "Almaburger - Nuovo messaggio da ".$_POST["nome"];
    $body = $_POST["messaggio"]."<br>";
    $headers = "From: ".$_POST["email"]."\r\n";
    $headers .= "Reply-To: ".$_POST["email"]."\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "Content-Transfer-Encoding: 8bit\r\n";
    mail($to, $subject, $body, $headers);
    header("Location: contact.php?success=1");
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Contattaci</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['success'])) {
              echo '<p class="text-center alert alert-success">Messaggio inviato con successo.</p><br>';
          }
          ?>
        <h1>Contattaci</h1>
        <p>Per qualsiasi domanda siamo pronti a rispondere!</p>
        <fieldset class= "border border-light mt-2">
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" method="post" action="contact.php">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" placeholder="Inserisci il tuo nome" class="form-control" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" placeholder="Inserisci la tua email" class="form-control" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="messaggio">Messaggio</label>
                  <textarea class="form-control" rows="6" style="resize:none;" name="messaggio" placeholder="Il tuo messaggio" id="messaggio" required></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Invia</button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $.validator.addMethod('check_email', function (value) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      }, 'Inserisci un indirizzo email valido');

      $.validator.addMethod('check_names', function (value) {
          return /^[A-Za-z ]+$/.test(value);
      }, 'Inserisci un nome valido');

      $("#form").validate({
         rules: {
            email: {
                check_email: true
            },
            nome: {
                check_names: true
            }
         }
       });
    </script>
  </body>
</html>
