<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
  }
  if(is_admin()) {
    header('Location: index.php');
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <style>
      .table > tbody > tr > td {
      vertical-align: middle;
      border: 0px;
      }
    </style>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-notify.min.js"></script>
    <title>Carrello</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <h1 class="mb-4 text-left">Carrello</h1>
        <?php
          $totale = 0;
          $stmt = $mysqli->prepare("SELECT id, nome, descrizione, immagine, prezzo, quantita FROM carrelli INNER JOIN menu ON menu.id = carrelli.idProdotto WHERE idUtente = ?");
          $stmt->bind_param("i", $_SESSION['user_id']);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($id, $nome, $descrizione, $immagine, $prezzo, $quantita);
          if($stmt->num_rows <= 0) {
              echo '<p class="text-center alert alert-info">Il tuo carrello sembra essere vuoto... corri a guardare il nostro menù per acquistare uno dei nostri buonissimi hamburger!<br><br><a class="btn btn-success" href="menu.php">Vai al menù!</a></p><br>';
          } else { ?>
        <table class="table table-striped" style="margin-left:-7px;">
          <thead>
            <tr class="text-center">
              <th class="d-none d-sm-table-cell"></th>
              <th>Prodotto</th>
              <th>Prezzo</th>
              <th>Quantità</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php
              while($stmt->fetch()) {
                $parziale = $prezzo * $quantita;
                $totale += $parziale; ?>
            <tr id="<?php echo $id; ?>" class="text-center align-middle">
              <td class="d-none d-sm-table-cell">
                <div class="row">
                  <div class="col-md-12"><img class="w-100" style="max-width:180px;" src="img/upload/<?php echo $immagine; ?>" alt="<?php echo $nome; ?>"></div>
                </div>
              </td>
              <td data-th="Prodotto"><?php echo $nome; ?></td>
              <td data-th="Prezzo"><?php echo $prezzo; ?>€</td>
              <form>
                <td data-th="Quantità">
                  <input type="number" class="qta_group" onchange="updateQuantity(this.form)" max="40" min="1" name="qta" value="<?php echo $quantita; ?>">
                </td>
                <td>
                  <input type="button" class="btn btn-danger fa fa-trash-o" onclick="deleteProduct(this.form)" value="&#xf014;">
                  <input type="number" name="idProd" value="<?php echo $id; ?>" hidden>
                </td>
              </form>
            </tr>
            <?php
              }
              ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="2">
                <h2>Totale <span id="lblTotale"><?php echo $totale; ?></span>€</h2>
              </td>
              <td class="text-right" colspan="3"><input type="button" onclick="location.href='menu.php'" class="btn btn-warning m-1" name="continueShopping" value="Continua a ordinare">
                <input type="button" onclick="location.href='checkout.php'" class="btn btn-success" name="checkout" value="Procedi al pagamento">
              </td>
            </tr>
          </tfoot>
        </table>
        <?php
          }
          ?>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $(document).ready(function () {
        $(".qta_group").keypress(function (e) {
          if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
          }
        });
      });

      function updateQuantity(form) {
        if(form.qta.value < 1) form.qta.value = 1;
        if(form.qta.value > 40) form.qta.value = 40;
        var idProd = form.idProd.value;
        var qta = form.qta.value;

        $.post("php/edit_cart.php", { idProd: idProd, qta: qta, action: "modifica" }, function(result) {
          if(result != "ERROR") {
            $('#lblTotale').text(result);
          } else {
            notifyError();
          }
        });
      }

      function deleteProduct(form) {
        var idProd = form.idProd.value;
        $.post("php/edit_cart.php", { idProd: idProd, action: "elimina" }, function(result) {
          if(result != "ERROR") {
            $('#' + idProd).remove();
            $('#lblTotale').text(result);
          } else {
            notifyError();
          }
        });
      }

      function notifyError() {
        $.notify({
          message: "Si è verificato un errore imprevisto..."
        },{
          type: "danger",
          offset: {
            x: 0,
            y: 10
          }
        });
      }
    </script>
  </body>
</html>
