<?php
  require 'php/functions.php';
  sec_session_start();
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-notify.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Menù</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['new'])) {
              echo '<p class="text-center alert alert-success">Inserimento avvenuto con successo</p><br>';
          } else if(isset($_GET['edit'])) {
              echo '<p class="text-center alert alert-success">Modifica avvenuta con successo</p><br>';
          } else if(isset($_GET['delete'])) {
              echo '<p class="text-center alert alert-success">Eliminazione avvenuta con successo</p><br>';
          }
          ?>
        <h1>Menù</h1>
        <div class="container-fluid content-row">
          <div class="row text-center">
            <div class="card mx-auto">
              <div class="card-header bg-primary">
                <h3 class="mb-0 text-center">Filtri</h3>
              </div>
              <div class="card-body justify-content-between">
                <form action="menu.php" method="get">
                  <label class="ml-1"><input type="checkbox" <?php if(isset($_GET['piccante'])) echo "checked" ?> name="piccante" value="P"/> Piccante</label>
                  <label class="ml-1"><input type="checkbox" <?php if(isset($_GET['glutine'])) echo "checked" ?> name="glutine" value="G"/> Senza glutine</label>
                  <label class="ml-1"><input type="checkbox" <?php if(isset($_GET['vegetariano'])) echo "checked" ?> name="vegetariano" value="V"/> Vegetariano</label><br>
                  <button type="submit" class="btn align-bottom btn-primary">
                  <em class="fa fa-filter" aria-hidden="true"> Filtra</em>
                  </button>
                </form>
              </div>
            </div>
          </div>
          <div class="row">
            <?php
              $imgFolder="img/upload/";
              $query = "SELECT id, nome, descrizione, immagine, prezzo FROM menu WHERE disponibile = 1 ";
              if(isset($_GET['piccante'])) $query .= " AND piccante = 1 ";
              if(isset($_GET['glutine'])) $query .= " AND senzaGlutine = 1 ";
              if(isset($_GET['vegetariano'])) $query .= " AND vegetariano = 1 ";
              if(isset($_GET['search'])) $query .= " AND nome LIKE '%".$_GET['search']."%'";
              $stmt = $mysqli->prepare($query);
              $stmt->execute();
              $stmt->store_result();
              if($stmt->num_rows > 0) {
                  $stmt->bind_result($id, $nome, $descrizione, $immagine, $prezzo);
                  while($stmt->fetch()) { ?>
            <div class="col-md-6 mt-2">
              <div class="card h-100">
                <div class="card-header bg-primary">
                  <h5 class="mb-0 text-center"><?php echo $nome; ?></h5>
                </div>
                <img class="img-fluid w-100" style="max-height:280px;" src="<?php echo $imgFolder.$immagine ?>" alt="<?php echo $nome; ?>">
                <div class="card-body text-center vcenter" style="padding-bottom:0px;">
                  <p><?php echo $descrizione; ?></p>
                  <h3><?php echo $prezzo; ?>€</h3>
                </div>
                <div class="card-footer force-to-bottom text-center">
                  <form action="edit-burger.php" method="post">
                    <input type="number" name="idProd" value="<?php echo $id; ?>" required hidden>
                    <?php
                      if(!is_admin()) {
                        echo '
                        <label>Quantità: <input type="number" name="quantita" value="1" min="1" max="20" required></label><br>
                        <button type="button" onclick="add2cart(this.form)" class="btn btn-success fa fa-shopping-cart"> Aggiungi al carrello</button>';
                      } else {
                      echo '
                        <button type="submit" name="modifica" class="btn btn-warning fa fa-pencil-square-o"> Modifica</button>
                        <button type="submit" name="elimina" onclick="conferma(event)" class="btn btn-danger fa fa-trash-o"> Elimina</button>';
                      }
                      ?>
                  </form>
                </div>
              </div>
            </div>
            <?php
              }
              }
              if(is_admin()) {
                echo '
              <div class="col-md-6 mt-2">
                <div class="card h-100">
                  <div class="card-header bg-primary">
                    <h5 class="mb-0 text-center">Aggiungi prodotto</h5>
                  </div>
                  <img class="img-fluid w-100" style="padding:20px 30px; max-height:350px;" src="img/res/new_hamburger.png">
                  <div class="card-body text-center vcenter" style="padding-bottom:0px;">
                    <p>Aggiungi un nuovo hamburger al menù</p>
                  </div>
                  <div class="card-footer force-to-bottom text-center">
                    <button type="button" onclick="location.href=\'edit-burger.php\'" class="btn btn-success fa fa-plus"> Inserisci</button>
                  </div>
                </div>
              </div>';
              }
              ?>
          </div>
        </div>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      function conferma(e) {
        if(!confirm('Sicuro di voler eliminare questo prodotto?'))e.preventDefault();
      }

      function add2cart(form) {
        $(form).validate();
        if($(form).valid()) {
          var qta = form.quantita.value;
          var idProd = form.idProd.value;
          $.post("php/add2cart.php", { idProd: idProd, qta: qta }, function(result){
            var messaggio = "";
            var tipo = "";
            switch(result) {
                case "NOLOGIN":
                    messaggio = "E' necessario effettuare prima il login per poter inserire prodotti nel carrello!";
                    tipo = "danger";
                    break;
                case "OK":
                    messaggio = "Prodotto inserito correttamente nel carrello!";
                    tipo = "success";
                    break;
                case "MAX":
                    messaggio = "E' possibile ordinare al massimo 40 unità per prodotto";
                    tipo = "danger";
                    break;
                default:
                    messaggio = "Si è verificato un errore inatteso...";
                    tipo = "danger";
            }
            $.notify({
              message: messaggio
            },{
              type: tipo,
              offset: {
                x: 0,
                y: 10
              }
            });
          });
        }
      }
    </script>
  </body>
</html>
