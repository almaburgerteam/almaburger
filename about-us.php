<?php
  require 'php/functions.php';
  sec_session_start();
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Chi siamo</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <div class="row">
          <div class="align-self-center p-3 col-md-6">
            <h1 class="mb-4 text-center">Chi Siamo</h1>
            <p class="mb-5 text-center">Almaburger è una consegna a domicilio di hamburger genuini. un pensiero sano che vive nelle cucine delle nonne e fra i sapori delle piccole botteghe di paese. &nbsp;un pensiero buono che nasce 40 anni fa nella nostra bottega di famiglia, a
              San Vito un paesino alla periferia di Rimini. un pensiero leggero pronto ad entrare oggi nelle case di tutte quelle persone convinte che un hamburger più sano, con amore, è possibile!
            </p>
          </div>
          <div class="col-md-6 p-0">
            <div id="carousel1" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <img class="d-block img-fluid w-100" src="img/res/burgermania-gallery-07.jpg" alt="I titolari">
                  <div class="carousel-caption">
                    <h2>I TITOLARI</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <?php require("footer.php"); ?>
  </body>
</html>
