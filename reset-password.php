<?php
  require 'php/functions.php';
  sec_session_start();

  if (isset($_POST['token']) && isset($_POST['pwd'])) {
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()) , true));
    $password = hash('sha512', $_POST['pwd'] . $random_salt);
    $stmt = $mysqli->prepare("UPDATE utenti SET token=?, password = ?, salt = ?, attivo=? WHERE token=?");
    $token="";
    $attivo = 1;
    $stmt->bind_param('sssds', $token, $password, $random_salt, $attivo, hash('sha512', $_POST['token']));
    $stmt->execute();
    logout();
    header('Location: login.php?reset=2');
    die();
  } else if(isset($_GET['key'])) {
    $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE token = ?");
    $stmt->bind_param('s', hash('sha512', ($_GET['key'])));
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows <= 0) {
      header('Location: index.php');
      die();
    }
  } else if (!isset($_GET['key'])) {
    header('Location: index.php');
    die();
  }

  $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE token = ?");
  $stmt->bind_param('d', hash('sha512', ($_GET['key'])));
  $stmt->execute();
  $stmt->store_result();
  if($stmt->num_rows <= 0) {
    header('Location: index.php');
    die();
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <title>Reset password</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <fieldset class= "border border-light mt-2">
          <legend  class="w-50 text-center">Reset password</legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" method="post" action="reset-password.php">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="pwd">Password</label>
                    <input type="password" name="pwd" id="pwd" minlength="6" placeholder="******" class="form-control" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="conf_pwd">Ripeti password</label>
                    <input type="password" name="conf_pwd" id="conf_pwd" minlength="6" placeholder="******" class="form-control" required>
                  </div>
                </div>
                <input type="text" name="token" value="<?php echo $_GET['key']; ?>" hidden>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Cambia password</button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $("#form").validate({
         rules: {
            conf_pwd: {
                equalTo: "#pwd"
            }
         }
       });
    </script>
  </body>
</html>
