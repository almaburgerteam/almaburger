-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Gen 28, 2018 alle 01:38
-- Versione del server: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_almaburger`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrelli`
--

CREATE TABLE IF NOT EXISTS `carrelli` (
  `idUtente` int(11) NOT NULL,
  `idProdotto` int(11) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `dettaglio_ordini`
--

CREATE TABLE IF NOT EXISTS `dettaglio_ordini` (
  `idOrdine` int(11) NOT NULL,
  `idProdotto` int(11) NOT NULL,
  `quantita` int(11) NOT NULL,
  `prezzo` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzi`
--

CREATE TABLE IF NOT EXISTS `indirizzi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUtente` int(11) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `indirizzo` varchar(50) NOT NULL,
  `comune` varchar(20) NOT NULL,
  `provincia` varchar(2) NOT NULL,
  `cap` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  `descrizione` varchar(200) NOT NULL,
  `immagine` varchar(40) NOT NULL,
  `prezzo` double NOT NULL,
  `piccante` tinyint(1) NOT NULL,
  `senzaGlutine` tinyint(1) NOT NULL,
  `vegetariano` tinyint(1) NOT NULL,
  `disponibile` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dump dei dati per la tabella `menu`
--

INSERT INTO `menu` (`id`, `nome`, `descrizione`, `immagine`, `prezzo`, `piccante`, `senzaGlutine`, `vegetariano`, `disponibile`) VALUES
(1, 'Apache', 'Hamburger con cipolla, pomodoro fresco e cetriolini servito con patate French Fries', 'FoA5OKGveiDf2X1ETwPNmgs64hlzLa.jpeg', 7.6, 0, 0, 0, 1),
(2, 'Navajo', 'Hamburger con formaggio cheddar servito con patate French Fries', 'MjaviYRlJQZqVgtbD10SzA4C9EWfHy.jpg', 7.7, 0, 0, 0, 1),
(3, 'Dakota', 'Hamburger con bacon grigliato e formaggio fuso servito con patate French Fries', 'kxNo9ltZPsC3hT1DKVuQpRrwB0Hdyq.jpg', 8.2, 0, 0, 0, 1),
(4, 'Nuvola Rossa', 'Hamburger con julienne di cipolla arrostita, formaggio fuso e bacon grigliato servito con patate French Fries', 'QPvyrdubTRtXq5fl6z48S7mDikh1Zp.jpg', 8.2, 0, 0, 0, 1),
(5, 'Wild Burger', 'Hamburger, formaggio Grana Padano, bacon grigliato, pomodoro, lattuga e salsa Old Wild West servito con patate French Fries', 'chWfanuekxKG17svVzwY3jpCPXQA0b.jpg', 8.7, 0, 0, 0, 1),
(6, 'BBQ Burger', 'Hamburger, bacon grigliato, anelli di cipolla, lattuga e salsa Barbecue, servito con patate Dippers e salsa Barbecue', 'PdNoDc2vbQs678BEkxwZMLq50pg4tj.jpg', 12, 0, 0, 0, 1),
(7, 'Toro Seduto', 'Hamburger, bacon grigliato, formaggio fuso e cipolla, servito con patate Dippers', 'msyfWv7i5jRA41q0TG8c6w3MDlXgUB.jpg', 11, 0, 0, 0, 1),
(8, 'American Burger', 'Hamburger, salsa Ketchup, insalata iceberg, pomodoro, formaggio cheddar, bacon grigliato, servito con patate Dippers', 'MJYjfXwruVWAvBoZCK0Q34ThI8FEUz.jpg', 13, 0, 0, 0, 1),
(9, 'Cow Burger', 'Hamburger, formaggio fuso, bacon croccante, cipolla caramellata, pomodoro, insalata iceberg e salsa Barbecue, servito con patate Dippers', '1yh3NRYJD9Fe2IQTukzOEbwdW8UaVM.png', 12, 0, 0, 0, 1),
(10, 'California Burger', 'Hamburger, formaggio fontina Val dâ€™Aosta, bacon croccante, pomodoro, spinacino, funghi e maionese/senape, servito con Dippers', 'b6HUlcWxwS1JPjL4EDeButn7gYA0Fh.png', 12.5, 0, 0, 0, 1),
(11, 'Country Burger', 'Hamburger, formaggio fontina Val dâ€™Aosta, bacon croccante, pomodorino secco, insalata, cappuccio rosso e salsa Old Wild West servito con Dippers', 'Hd4BLfYpq9CNvgMQerw8Jo5WAxXmsG.png', 11, 0, 0, 0, 1),
(12, 'French Burger', 'Hamburger, formaggio Camembert, bacon croccante, cipolla caramellata, spinacino e salsa Old Wild West servito con patate Dippers', 'SZTcF8HYouDaRi7IGg51jPQ4Ay3mzt.png', 11, 0, 0, 0, 1),
(13, 'Crispy Bacon', 'Cotoletta di pollo panata con insalata, pomodoro, bacon grigliato e salsa Caesar servito con patate French Fries', 'ov2KXZ4EwYz1VpmjIuarDMcAsxU9nL.jpg', 7.4, 0, 0, 0, 1),
(14, 'Raggio di Sole', 'Cotoletta di pollo panata con insalata, pomodoro e maionese servito con patate French Fries', 'itxE7MCFlobyQ9Y4qJs5TWpc3whGBa.jpg', 7.7, 0, 0, 0, 1),
(15, 'Chicken Creek', 'Petto di pollo alla griglia con bacon grigliato, formaggio fuso, insalata e salsa Old Wild West servito con patate French Fries', 'WcqRxz7OQk6Y4F1HgnsZuawKAvdt5M.jpg', 7.9, 0, 0, 0, 1),
(16, 'Veggie Burger', 'Burger di legumi e cereali, maionese veg e ketchup bio, insalata e pomodoro da agricoltura biologica', 'J1GFKYnCXylUb7ISjWweEiAh5kzTBx.png', 7.9, 0, 0, 1, 1),
(17, 'Angry Burger', 'Angry Burger include un tocco piccante grazie all''aggiunta di deliziosi jalapenos, ma senza perdere la sua essenza tradizionale. Viva il Messico!', 'i824UGpVRsmynzZ63e7JwXKNErLAul.png', 11.5, 1, 0, 0, 1),
(18, 'Gluten Free Burger', 'Due fette di formaggio filante si uniscono a due fette di gustosa carne bovina. Il tutto racchiuso in un morbido pane senza glutine.', 'uxZbm2vfSq6jJcpQXB1nWRwkDdr9gT.png', 8, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE IF NOT EXISTS `notifiche` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUtente` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `testo` varchar(500) NOT NULL DEFAULT '0',
  `letta` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE IF NOT EXISTS `ordini` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUtente` int(11) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `indirizzo` varchar(50) NOT NULL,
  `comune` varchar(20) NOT NULL,
  `provincia` varchar(2) NOT NULL,
  `cap` varchar(5) NOT NULL,
  `data` datetime NOT NULL,
  `metodoPagamento` varchar(20) NOT NULL,
  `stato` varchar(20) NOT NULL,
  `recensione` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE IF NOT EXISTS `utenti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `token` varchar(128) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `codFisc` varchar(16) DEFAULT NULL,
  `immagine` varchar(40) NOT NULL DEFAULT 'default.jpg',
  `attivo` tinyint(1) NOT NULL DEFAULT '0',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
