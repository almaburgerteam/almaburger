<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
  }

  if(isset($_POST['elimina'])) {
    $stmt = $mysqli->prepare("SELECT idUtente FROM indirizzi WHERE id = ?");
    $stmt->bind_param('d', $_POST["idIndirizzo"]);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows <= 0) {    //Controllo che l'indirizzo esista
      header("location: profile.php");
      die();
    } else {
      $stmt->bind_result($id);
      $stmt->fetch();
      if($id != $_SESSION['user_id']) {   //Controllo che l'indirizzo sia dell'utente corrente
        header("location: profile.php?error=6");
        die();
      }
    }
    //Controllo che non sia l'unico indirizzo
    $stmt = $mysqli->prepare("SELECT id FROM indirizzi WHERE idUtente = ?");
    $stmt->bind_param('d', $_SESSION['user_id']);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == 1) {
      header("location: profile.php?error=7");
      die();
    }
    $stmt = $mysqli->prepare("DELETE FROM indirizzi WHERE id = ?");
    $stmt->bind_param('d', $_POST["idIndirizzo"]);
    $stmt->execute();
    header("location: profile.php?success=4");
  }

  if(isset($_POST['modifica'])) {
    $stmt = $mysqli->prepare("SELECT idUtente, cognome, nome, indirizzo, comune, provincia, cap FROM indirizzi WHERE id = ?");
    $stmt->bind_param('d', $_POST['idIndirizzo']);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 0) {
        $stmt->bind_result($idUtente, $cognome, $nome, $indirizzo, $comune, $provincia, $cap);
        $stmt->fetch();
        if($idUtente != $_SESSION['user_id']) {
          header("location: profile.php?error=6");
          die();
        }
    } else {
      header('Location: profile.php');
      die();
    }
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <?php echo isset($_POST['idIndirizzo']) ? "<title>Modifica indirizzo di spedizione</title>" : "<title>Aggiungi indirizzo di spedizione</title>"; ?>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <fieldset class= "border border-light">
          <legend  class="w-50 text-center" ><?php echo isset($_POST['idIndirizzo']) ? "Modifica indirizzo di spedizione" : "Aggiungi indirizzo di spedizione"; ?></legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" method="post" action="php/edit_address.php">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" value="<?php echo $nome; ?>" placeholder="Inserisci il tuo nome" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="cognome">Cognome</label>
                    <input type="text" name="cognome" id="cognome" value="<?php echo $cognome; ?>" placeholder="Inserisci il tuo cognome" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="indirizzo">Indirizzo</label>
                  <input type="text" name="indirizzo" id="indirizzo" value="<?php echo $indirizzo; ?>" placeholder="Inserisci il tuo indirizzo" class="form-control" required>
                </div>
                <div class="row">
                  <div class="col-sm-4 form-group">
                    <label for="comune">Comune</label>
                    <input type="text" name="comune" id="comune" value="<?php echo $comune; ?>" placeholder="Inserisci il tuo comune" class="form-control" required>
                  </div>
                  <div class="col-sm-4 form-group">
                    <label for="provincia">Provincia</label>
                    <input type="text" name="provincia" minlength="2" maxlength="2" value="<?php echo $provincia; ?>" id="provincia" placeholder="Inserisci la tua provincia" class="form-control" required>
                  </div>
                  <div class="col-sm-4 form-group">
                    <label for="cap">CAP</label>
                    <input type="text" name="cap" id="cap" minlength="5" maxlength="5" value="<?php echo $cap; ?>" placeholder="Inserisci il tuo CAP" class="form-control" required>
                  </div>
                </div>
                <input type="number" name="id" value="<?php echo $_POST['idIndirizzo'] ?>" hidden>
                <button type="submit" class="btn align-bottom btn-primary btn-lg btn-block">Salva</button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $.validator.addMethod('check_names', function (value) {
          return /^[A-Za-z ]+$/.test(value);
      }, 'Inserisci un nome valido');

      $.validator.addMethod('check_cap', function (value) {
          return /^\d{5}$/.test(value);
      }, 'Inserisci un CAP valido');

      $("#form").validate({
         rules: {
            nome: {
                check_names: true
            },
            cognome: {
                check_names: true
            },
            cap: {
                check_cap: true
            }
         }
       });
    </script>
  </body>
</html>
