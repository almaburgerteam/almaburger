<?php
  require 'php/functions.php';
  sec_session_start();
  if (!is_admin()) {
    header('Location: index.php');
    die();
  }

  if(isset($_POST['elimina'])) {
    $no = 0;
    $stmt = $mysqli->prepare("UPDATE menu SET disponibile = ? WHERE id = ?");
    $stmt->bind_param('dd', $no, $_POST["idProd"]);
    $stmt->execute();
    $stmt = $mysqli->prepare("DELETE FROM carrelli WHERE idProdotto = ?");
    $stmt->bind_param('d', $_POST["idProd"]);
    $stmt->execute();
    header("location: menu.php?delete=1");
  }

  if(isset($_POST['modifica'])) {
    $stmt = $mysqli->prepare("SELECT id, nome, descrizione, prezzo, piccante, senzaGlutine, vegetariano FROM menu WHERE id = ?");
    $stmt->bind_param('d', $_POST['idProd']);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 0) {
        $stmt->bind_result($id, $nome, $descrizione, $prezzo, $piccante, $glutine, $vegetariano);
        $stmt->fetch();
    } else {
      header('Location: menu.php');
      die();
    }
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/messages_it.min.js"></script>
    <?php echo isset($_POST['idProd']) ? "<title>Modifica Hamburger</title>" : "<title>Aggiungi Hamburger</title>"; ?>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <?php
          if(isset($_GET['error'])) {
              echo '<p class="text-center alert alert-danger">Inserimento fallito</p><br>';
          }
          ?>
        <fieldset class= "border border-light mt-2">
          <legend  class="w-50 text-center"><?php echo isset($_POST['idProd']) ? "Modifica hambuger" : "Aggiungi un nuovo hambuger"; ?></legend>
          <div class="card card-body bg-secondary">
            <div class="row">
              <form class="col-sm-12" id="form" enctype="multipart/form-data" method="post" action="php/edit_menu.php">
                <div class="row">
                  <div class="col-sm-5 form-group">
                    <label for="nome">Nome</label>
                    <input type="number" name="id" value="<?php echo $_POST['idProd'] ?>" hidden>
                    <input type="text" value="<?php echo $nome ?>" maxlength="40" name="nome" id="nome" placeholder="Nome" class="form-control" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-9 form-group">
                    <label for="descrizione">Descrizione</label>
                    <textarea class="form-control" maxlength="200" rows="5" style="resize:none;" name="descrizione" placeholder="Descrizione del piatto: elenca tutti gli ingredienti" id="descrizione" required><?php echo $descrizione ?></textarea>
                  </div>
                  <div class="col-sm-3 form-group">
                    <label>Varietà</label>
                    <div class="checkbox">
                      <label><input type="checkbox" <?php if($piccante) echo "checked" ?> name="piccante" value="P"/> Piccante</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" <?php if($glutine) echo "checked" ?> name="glutine" value="G"/> Senza glutine</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" <?php if($vegetariano) echo "checked" ?> name="vegetariano" value="V"/> Vegetariano</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="immagine">Immagine</label>
                    <input type="file" name="immagine" id="immagine" class="form-control" <?php if(!isset($_POST['idProd'])) echo "required"; ?>>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="prezzo">Prezzo</label>
                    <input type="number" value="<?php echo $prezzo ?>" name="prezzo" id="prezzo" placeholder="Prezzo" class="form-control" required>
                  </div>
                </div>
                <button type="submit" class="btn align-bottom btn-primary btn-lg btn-block"><?php echo isset($_POST['idProd']) ? "Modifica" : "Inserisci"; ?></button>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <script>
      $("#form").validate({
        rules: {
          immagine: {
            accept: "image/*"
          }
        },
        messages: {
          immagine: "Carica un'immagine valida"
        }
      });
    </script>
  </body>
</html>
