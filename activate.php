<?php
  require 'php/functions.php';

  if(isset($_GET['key'])) {
    $stmt = $mysqli->prepare("SELECT id FROM utenti WHERE token = ?");
    $stmt->bind_param('d', hash('sha512', ($_GET['key'])));
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows <= 0) {
      header('Location: login.php?error=invalid');
      die();
    }
    $stmt = $mysqli->prepare("UPDATE utenti SET token=?, attivo=? WHERE token=?");
    $token="";
    $attivo = 1;
    $stmt->bind_param('sds', $token, $attivo, hash('sha512', ($_GET['key'])));
    $stmt->execute();
    header('Location: login.php?new=2');
  } else {
    header('Location: login.php?error=invalid');
  }
?>
