<footer class="footer">
  <div class="container bg-dark text-white">
    <div class="row">
      <div class="p-4 col-md-6">
        <h2 class="mb-2 text-light">Contatti</h2>
        <p><em class="fa d-inline mr-2 text-secondary fa-phone"></em> 0547338850 </p>
        <p><em class="fa d-inline mr-2 text-secondary fa-envelope-o"></em>info@almaburger.com</p>
        <p><em class="fa d-inline mr-2 fa-map-marker text-secondary"></em>&nbsp;&nbsp;Via Sacchi 3, Cesena</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2863.265919653211!2d12.241030615513566!3d44.13976147910756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132ca4c8a9e0b5cb%3A0xc2f45d35a4d7432c!2sVia+Sacchi%2C+3%2C+47521+Cesena+FC!5e0!3m2!1sit!2sit!4v1516095235105" height="250" style="border:0"></iframe>
      </div>
      <div class="p-4 col-md-6">
        <h2 class="mb-2 text-light">Metodi di pagamento</h2>
        <div class="iconpay mb-4">
          <em class="fa fa-3x fa-cc-mastercard mr-2" style="color:#aaaaaa;"></em>
          <em class="fa fa-3x fa-cc-visa mr-2" style="color:#aaaaaa;"></em>
          <em class="fa fa-3x fa-cc-paypal mr-2" style="color:#aaaaaa;"></em>
          <em class="fa fa-3x fa-btc" style="color:#aaaaaa;"></em>
        </div>
        <h2 class="mb-2 text-light">Social</h2>
        <div class="iconsocial">
          <a href="https://www.facebook.com" target="_blank"><em class="fa fa-3x fa-facebook-square zoom mr-2"></em></a>
          <a href="https://www.twitter.com" target="_blank"><em class="fa fa-3x fa-twitter-square zoom mr-2"></em></a>
          <a href="https://www.instagram.com" target="_blank"><em class="fa fa-3x fa-instagram zoom mr-2"></em></a>
          <a href="https://www.telegram.org" target="_blank"><em class="fa fa-3x fa-telegram zoom"></em></a>
        </div>
      </div>
    </div>
    <div class="row">
      <p class="mx-auto">© Copyright 2018 Almaburger - Tutti i diritti riservati.</p>
    </div>
  </div>
</footer>
