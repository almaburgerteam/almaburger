<?php
  require 'php/functions.php';
  sec_session_start();
  if (!login_check()) {
    header('Location: login.php?error=required');
    die();
  }
  ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Notifiche</title>
  </head>
  <body class="bg-dark">
    <?php require("navbar.php"); ?>
    <main class="py-3 text-white bg-secondary">
      <div class="container">
        <h2>Notifiche</h2>
        <?php
          $stmt = $mysqli->prepare("SELECT data, testo FROM notifiche WHERE idUtente = ? AND letta = 0 ORDER BY id DESC");
          $stmt->bind_param('i', $_SESSION['user_id']);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($data, $testo);
          if($stmt->num_rows <= 0) {
            echo '<p class="text-center alert alert-info">Non ci sono nuove notifiche da leggere</p>';
          } else {
          ?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Data</th>
              <th>Notifica</th>
            </tr>
          </thead>
          <tbody>
            <?php
              while($stmt->fetch()) {
                $date = new DateTime($data);
                $dataFormattata = $date->format('d/m/Y H:i');?>
            <tr>
              <td data-th="Data"><?php echo $dataFormattata; ?></td>
              <td data-th="Notifica"><?php echo $testo; ?></td>
            </tr>
            <?php
              } ?>
          </tbody>
        </table>
        <? } ?>
      </div>
    </main>
    <?php require("footer.php"); ?>
    <?php
      $stmt = $mysqli->prepare("UPDATE notifiche SET letta = 1 WHERE idUtente = ?");
      $stmt->bind_param('i', $_SESSION['user_id']);
      $stmt->execute();
      ?>
  </body>
</html>
